#!/usr/bin/python3

output_name = "output.scad";
sections = list();

with open("input.scad","r") as filehandle:
    lines = filehandle.readlines();

for i in range(len(lines)):
    if "//--" in lines[i]:
        if "//--//" in lines[i]:
            sections.append(list());
            
        sections[-1].append('');
    else:
        if i == 0 :
            sections.append(list());
            sections[0].append('');

        sections[-1][-1] += lines[i];

i = 0;
while i < len(sections):
    j = 0;
    outputfile = open(output_name, "w");
    last_input = '';
    while 0 <= j < len(sections[i]):
        if 'u' in last_input:
            outputfile.close();
            outputfile = open(output_name, "w");
            for k in range(j+1):
                outputfile.write( sections[i][k]);
        else:
            outputfile.write( sections[i][j] );
        
        outputfile.flush();
        print("type a 'u' to go back, type enter to go forward \n");
        last_input = input();
        if "u" in last_input:
            j -= 1;
        else:
            j += 1;
    
    outputfile.close();
    if j < 0 and i > 0:
        i -= 1;
    elif j >= 0:
        i += 1;

